
function disabledButtons(){
    var buttons = document.querySelectorAll('.btn');
    for (var i = 0; i < buttons.length; i++){
        buttons[i].disabled = true;
        buttons[i].classList.add('disabled');
    }
}

function enabledButtons(){
    var buttons = document.querySelectorAll('.btn');
    for (var i = 0; i < buttons.length; i++){
        buttons[i].disabled = false;
        buttons[i].classList.remove('disabled');
    }
}
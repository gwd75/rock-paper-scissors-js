/*
 0 : rock
 1 : papers
 2 : scissors
*/

function reset(){
    resetScore()
    document.getElementById("computer-choice-img").src="/../images/play.svg"
}

function play(index){
    const computerChoice = Math.floor(Math.random() * 3)
    switch(index){
        case 0:
            if(computerChoice === 0){
                document.getElementById("computer-choice-img").src="/../images/rock.svg";
                hasDraw()
            } else if(computerChoice === 1){
                document.getElementById("computer-choice-img").src="/../images/papers.svg";
                hasLose()
            } else if(computerChoice === 2){
                document.getElementById("computer-choice-img").src="/../images/scissors.svg";
                hasWin()
            }
            break;
        case 1:
            if(computerChoice === 0){
                document.getElementById("computer-choice-img").src="/../images/rock.svg";
                hasWin()
            } else if(computerChoice === 1){
                document.getElementById("computer-choice-img").src="/../images/papers.svg";
                hasDraw()
            } else if(computerChoice === 2){
                document.getElementById("computer-choice-img").src="/../images/scissors.svg";
                hasLose()
            }
            break;
        case 2:
            if(computerChoice === 0){
                document.getElementById("computer-choice-img").src="/../images/rock.svg";
                hasLose()
            } else if(computerChoice === 1){
                document.getElementById("computer-choice-img").src="/../images/papers.svg";
                hasWin()
            } else if(computerChoice === 2){
                document.getElementById("computer-choice-img").src="/../images/scissors.svg";
                hasDraw()
            }
            break;
    }
}

function handleClick(index){
    disabledButtons()
    setTimeout(function(){
        play(index)
        enabledButtons()
    },1000)
}
var myScore = 0
var compScore = 0

function resetScore(){
    myScore = 0
    compScore = 0
    document.getElementById('your-score').innerHTML = 0
    document.getElementById('computer-score').innerHTML = 0
    document.getElementById('result').innerHTML = ''
    document.getElementById('result').removeAttribute('class');
}

function hasLose(){
    console.log('défaite');
    compScore = compScore + 1
    document.getElementById('computer-score').innerHTML = compScore 
    document.getElementById('result').innerHTML = 'Perdu !'
    document.getElementById('result').removeAttribute('class');
    document.getElementById('result').classList.add('lose');
}

function hasDraw(){
    console.log('match nul');
    document.getElementById('result').innerHTML = 'Match Nul !'
    document.getElementById('result').removeAttribute('class');
}

function hasWin(){
    console.log('victoire');
    myScore = myScore + 1
    document.getElementById('your-score').innerHTML = myScore 
    document.getElementById('result').innerHTML = 'Gagné !'
    document.getElementById('result').removeAttribute('class');
    document.getElementById('result').classList.add('win');
}